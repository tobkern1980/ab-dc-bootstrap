#!/bin/bash
USER_CEPH=cephuser

sudo apt install ntp

sudo useradd -d /home/$USER_CEPH -m $USER_CEPH
sudo passwd $USER_CEPH

echo "cephuser  ALL = (root) NOPASSWD:ALL" | tee /etc/sudoers.d/cephuser

# Copy ssh id on all nodes

for node in $NODES[@]
  do
    ssh-copy-id $node
done

# Open FW Ports
#
# mon on CentOS
# sudo firewall-cmd --zone=public --add-service=ceph-mon --permanent
#
# sudo firewall-cmd --zone=public --add-service=ceph --permanent
# sudo firewall-cmd --reload


# iptabels
# port 6789 for Ceph Monitors and ports 6800:7300 for Ceph OSDs
# sudo iptables -A INPUT -i {iface} -p tcp -s {ip-address}/{netmask} --dport 6789 -j ACCEPT
# /sbin/service iptables save

MON_NODES=(mon-01)

OSD_NODES=(osd-01 osd-02 osd-03)

cat << EOF >> ~/.ssh/config
Host ceph-mon-01
    Hostname ceph-mon-01
    User cephuser

Host ceph-osd-01
    Hostname ceph-osd-01
    User cephuser

Host ceph-osd-02
    Hostname ceph-osd-02
    User cephuser

Host ceph-osd-03
    Hostname ceph-osd-03
    User cephuser

EOF

chmod 600 ~/.ssh/config

for host in $hosts ; do
    scp /etc/hosts $host:/etc/hosts
done

for host in $hosts
  do
    scp /etc/yum.repos.d/ceph.repo $host:/etc/yum.repos.d/ceph.repo
done

ceph-deploy new mon-01 --public-network 192.168.4.0/24 10.10.20.0/24

#
ceph-deploy install mon-01 osd-01 osd-02 osd-03 --release=nautilus

# osd deployen
ceph-deploy osd create osd-01 --data /dev/vdb

