#!/bin/bash -e
PROXMOX_USER=$1
PROXMOX_PASS=$2
PROXMOX_NODE=$3
PROXMOX_TARGET_NODE=$4

curl  --insecure --cookie "$(<cookie)" https://${PROXMOX_NODE}:8006/api2/json/nodes/${PROXMOX_TARGET_NODE}/status | jq '.'