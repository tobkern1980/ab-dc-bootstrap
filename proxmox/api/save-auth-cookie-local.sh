#!/bin/bash
PROXMOX_USER=$1
PROXMOX_PASS=$2
PROXMOX_NODE=$3

# requirements:
# - jq

curl --silent --insecure --data "username=${PROXMOX_USER}@pam&password=${PROXMOX_PASS}" \
 https://${PROXMOX_NODE}:8006/api2/json/access/ticket\
| jq --raw-output '.data.ticket' | sed 's/^/PVEAuthCookie=/' > cookie
