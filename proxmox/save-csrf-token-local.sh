#!/bin/bash
PROXMOX_USER=$1
PROXMOX_PASS=$2
PROXMOX_NODE=$3


curl --silent --insecure --data "username=${PROXMOX_USER}@pam&password=${PROXMOX_PASS}" \
 https://${PROXMOX_NODE}:8006/api2/json/access/ticket \
| jq --raw-output '.data.CSRFPreventionToken' | sed 's/^/CSRFPreventionToken:/' > csrftoken