#!/bin/bash

dpkg-reconfigure locales

cat << EOF >> /etc/hosts
10.10.10.11 pve6-c1.cluster.local pve6-c1
10.10.10.12 pve6-c2.cluster.local pve6-c2
10.10.10.13 pve6-c2.cluster.local pve6-c3
EOF

ping -c 1 server1

cat << EOF >>/etc/apt/sources.list.d/pve.list
wget http://download.proxmox.com/debian/proxmox-ve-release-5.x.gpg -O /etc/apt/trusted.gpg.d/proxmox-ve-release-5.x.gpg
wget http://download.proxmox.com/debian/ceph-nautilus/dists/buster/Release.gpg -O /etc/apt/trusted.gpg.d/pve5-cepth.gpg
EOF

apt-get update
apt-get upgrade

rm -f /etc/apt/sources.list.d/pve-enterprise.list

# PVE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve buster pve-no-subscription

wget  http://download.proxmox.com/debian/key.asc && apt-key add key.asc

dpkg-reconfigure locales

# Cluster anlegen auf pve6-c1

pvecm create cluster01

#on pve6-c2 and pve6-c3

pvecm add pve6-c1
pvecm status

# MAke Ceph Cluster for Proxmox

pveceph install

# nur auf einem system
pveceph init --network 10.10.10.0/24

pveceph createmon
pveceph createosd /dev/vdb

ceph osd tree