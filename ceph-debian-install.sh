#!/bin/bash

codename=$(cat /etc/os-release | grep "VERSION_CODENAME" | cut -d "=" -f 2)

# gnupgp2 fehlt um den apt repo key einrichten zu könen
apt-get install tmux aptitude vim gnupg2

# Add Key for ceph repos
wget -q -O- 'https://download.ceph.com/keys/release.asc' > ceph.key && apt-key add ceph.key

# add Ceph repo
echo "deb https://download.ceph.com/debian-luminous/ ${codename} main" > /etc/apt/sources.list.d/ceph.list
