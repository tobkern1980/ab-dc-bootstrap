#!/bin/bash

mkdir $(pwd)/wgkeys
cd wgkeys

wg genkey > server_$(hostname -s).key
wg pubkey > server-$(hostname)-public.key < server-$(hostname -s)-private.key
wg genkey > client1_private.key
chmod 770 client1_private.key
wg pubkey > client1_public.key < client1_private.key