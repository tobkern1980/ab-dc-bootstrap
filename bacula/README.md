# Ansible roles

Via git

* (restore-db-and-files)[https://github.com/stancel/restore-db-and-files.git]
* (add-job-to-bareos-director)[https://github.com/stancel/add-job-to-bareos-director.git]
* (restore-bareos-backup)[https://github.com/stancel/restore-bareos-backup.git]

via Galaxy

`ansible-galaxy install stancel.restore_db_and_files`

`ansible-galaxy install stancel.add-job-to-bareos-director`

`ansible-galaxy install stancel.restore_bareos_backup`


https://www.svennd.be/adding-a-linux-client-to-bareos/
https://www.bareos.org/en/HOWTO/articles/set_up_backup_client.html
