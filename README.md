# Links

* [ansible-role-provision-proxmox](https://github.com/bihealth/ansible-role-provision-proxmox)
* [ansible-role-proxmox](https://github.com/manala/ansible-role-proxmox)
* [ansible-role-proxmox cluster](https://github.com/lae/ansible-role-proxmox)
* [ansible-role-proxmox-vms](https://github.com/inoxio/ansible-role-proxmox-vms)
* [Build Devel VIrtaul Lab with Proxmox](https://www.gastongonzalez.com/tech-blog/2016/12/16/building-a-developer-virtualization-lab-part-1-1)
* [Vagrant Proxmox Box](https://app.vagrantup.com/phaus/boxes/proxmox)
* [Vagrant Proxmox Plugin](https://github.com/rgl/proxmox-ve)
* [Proxmox Installation mit Vagrant](https://lunar.computer/posts/vagrant-proxmox-60/)
* [NetBox](https://github.com/netbox-community/netbox)
* [NetBox Ansible Collections](https://github.com/netbox-community/ansible_modules.git)
* [Samlung von Ansible Rollen und skripten](https://github.com/ome/ansible-roles)
* [Ansible APB](https://github.com/automationbroker/apb)
* [Proxmox VE Vagrant Base Box](https://github.com/rgl/proxmox-ve.git)
* [Ceph OSD](https://github.com/alvistack/ansible-role-ceph_osd.git)
* [Ansible Module lenovo cnos vlan module](https://docs.ansible.com/ansible/latest/modules/cnos_vlan_module.html#cnos-vlan-module)
* [lenovo lxca-inventory](https://galaxy.ansible.com/lenovo/lxca-inventory)
* [ipmi](https://github.com/adfinis-sygroup/ansible-role-ipmi.git)

[Release notes](https://pve.proxmox.com/wiki/Roadmap#Proxmox_VE_6.1)
[Video intro](https://www.proxmox.com/en/training/video-tutorials/item/what-s-new-in-proxmox-ve-6-1)
[Proxmox Download](https://www.proxmox.com/en/downloads)
[Alternate ISO download](http://download.proxmox.com/iso/)
[Documentation](https://pve.proxmox.com/pve-docs/)
[ommunity Forum](https://forum.proxmox.com)
[Source Code](https://git.proxmox.com)
[Bugtracker](https://bugzilla.proxmox.com)
[PVE Upgrade von Version 5 zu](https://pve.proxmox.com/wiki/Upgrade_from_5.x_to_6.0)
[Upgrade Ceph von Luminous zu Nautilus](https://pve.proxmox.com/wiki/Ceph_Luminous_to_Nautilus)
[PVE Dokumentation](https://pve.proxmox.com/pve-docs/)

## Systeme

### ansible.albertbauer.com

Geht nur via root@admin

### monitor.albertbauer.com

https://github.com/Icinga/ansible-playbooks/tree/master/icinga2-ansible-add-hosts

### elk-srv.albertbauer.com

Gefunden in /etc/ansible/vars/elk-srv.yml
???

## Wireguard

Geht nur via root@admin dort gibt es dann immmer den zwang zur

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Thu May  7 16:42:28 2020 from 172.20.3.21
Auflistung... Fertig

 cd /etc/wireguard/clients...
 
 uid=<uid> && wg genkey | tee $uid.privatekey | wg pubkey > $uid.publickey
 user config und wg config anpassen (und dienst neu starten)
tkern.conf

## Proxmox Migration

Stefan Buchhop, [08.05.20 14:39]
Configuration
VM configuration files are stored inside the Proxmox cluster file system, and can be accessed at /etc/pve/qemu-server/<VMID>.conf. Like other files stored inside /etc/pve/, they get automatically replicated to all other cluster nodes.

Stefan Buchhop, [08.05.20 14:40]
https://pve.proxmox.com/pve-docs/chapter-qm.html#_managing_virtual_machines_with_span_class_monospaced_qm_span

### Proxmox System Netzwerk

prox-01
Name        Typ             Aktiv    Autostart     IP Address          Maske                    Gate    Kommentar
eth0        NIC             yes      yes           172.20.2.61         255.255.255.0
eth1        OVS Port        Yes      no
eth2        NIC             NO       No
eth3        NIC             NO       No
vlan23      OVS IntPort     Yes      No
vmbr0       OVS Bridge      No       Yes

prox-02
Name        Typ             Aktiv    Autostart    Ports/Slaves         IP Address          Maske               Gate  Kommentar
eth0        NIC             yes      yes                               172.20.2.62         255.255.255.0
eth1        OVS Port        Yes      no
eth2        NIC             NO       No
eth3        NIC             NO       No
vlan23      OVS IntPort     Yes      No                                172.20.3.62         255.255.255.0       172.20.3.254
vmbr0       OVS Bridge      No       Yes          eth1 vlan23

prox-03
Name        Typ         Aktiv    Autostart  IP Address          Maske           Gate    Kommentar

prox-04
Name        Typ         Aktiv    Autostart  IP Address          Maske           Gate    Kommentar

prox-05
Name        Typ         Aktiv    Autostart  IP Address          Maske           Gate

* [ProxBash](https://raymii.org/s/software/ProxBash.html)

> Bis welcher Proxmox Version ?

## Server Supermicro

* [ipmi-utilities](https://www.supermicro.com/en/solutions/management-software/ipmi-utilities)
* [Supermicro Login](https://www.supermicro.com/SwDownload/SwSelect_Free.aspx?cat=IPMI)

Usage: IPMICFG params (Example: IPMICFG -m 192.168.1.123)
  -m                    Show IP and MAC.
  -m IP                 Set IP (format: ###.###.###.###).
  -a MAC                Set MAC (format: ##:##:##:##:##:##).
  -k                    Show Subnet Mask.
  -k Mask               Set Subnet Mask (format: ###.###.###.###).
  -dhcp                 Get the DHCP status.
  -dhcp on              Enable the DHCP.
  -dhcp off             Disable the DHCP.
  -g                    Show Gateway IP.
  -g IP                 Set Gateway IP (format: ###.###.###.###).
  -r                    BMC cold reset.
                        option: -d | Detected IPMI device for BMC reset.
  -garp on              Enable the Gratuitous ARP.
  -garp off             Disable the Gratuitous ARP.
  -fd                   Reset to the factory default.
                        option: -d | Detected IPMI device for BMC reset.
  -fdl                  Reset to the factory default. (Clean LAN)
                        option: -d | Detected IPMI device for BMC reset.
  -fde                  Reset to the factory default. (Clean FRU & LAN)
                        option: -d | Detected IPMI device for BMC reset.
  -ver                  Get Firmware revision.
  -vlan                 Get VLAN status.
  -vlan on <vlan tag>   Enable the VLAN and set the VLAN tag.
                        If VLANtag is not given it uses previously saved value.

## Server Dell

idarce

## ldap

* [Ansible ldap lib](https://github.com/karlmdavis/ansible-role-ldap)
* [Ansible entry module](https://docs.ansible.com/ansible/latest/modules/ldap_entry_module.html)
* [Ansible attr Module](https://docs.ansible.com/ansible/latest/modules/ldap_attr_module.html)
* [Ansible ldap Passwd Module]([ldap_passwd](https://docs.ansible.com/ansible/latest/modules/ldap_passwd_module.html)

## Ansible  Packet Management

* [Ansible Update managemnt](https://github.com/stafwag/ansible-role-package_update)
* [Apt managemnt](https://github.com/Oefenweb/ansible-apt)
* [Repo management](https://github.com/mablanco/ansible-repomgmt.git)

## Ansible motd

* [serverfaut](https://serverfault.com/questions/459229/is-it-possible-to-put-commands-in-etc-motd/459737)
* [Sysinfo script](https://ownyourbits.com/2017/04/05/customize-your-motd-login-message-in-debian-and-ubuntu/)
* [ansible-motd mrlesmithj](https://github.com/mrlesmithjr/ansible-motd/blob/master/templates/etc/motd.sh.j2)
* [ansible motd arillso](https://github.com/arillso/ansible.motd)

## ShellVideos

* [asciima](https://www.linux-community.de/ausgaben/linuxuser/2020/03/shelltube/)