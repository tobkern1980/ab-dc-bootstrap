#!/bin/bash -e
#PWD_ROLES = $(pwd)
#ROLES_DIR = $PWD_ROLES/roles

RIGHT_SIDE = "' }"
LEFT_SIDE = "    - { name: \""
MIDDLE = "', dest: '"

ROLE_NAME = ""

File_REPOS = "./ansible-repos.list"

GET_CHANGED = $(git pull --dry-run | grep -q -v 'Already up-to-date.' && CHANGED="1" && export $CHANGED)

GET_FILE_REPOS () {
        while IFS= read -r line
            do
            REPO_NAME = "$line"
            echo "roles/$line"
        done < "$File_REPOS"
}

GET_FILE_REPOS