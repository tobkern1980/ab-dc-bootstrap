#!/usr/bin/env python3
# https://code-examples.net/de/docs/docker~17/engine/admin/logging/journald/index

import systemd.journal

read_container = raw_input("Name of the container")

reader = systemd.journal.Reader()
#reader.add_match(CONTAINER_NAME=input())
reader.add_match(CONTAINER_NAME=(read_container))

    for msg in reader:
      print '{CONTAINER_ID_FULL}: {MESSAGE}'.format(**msg)