#!/bin/bash
# https://docs.ansible.com/ansible/latest/user_guide/vault.html#viewing-encrypted-files
ansible-vault encrypt /home/$USER/.ansible/secret
read $PW
echo "$PW" > /home/$USER/.ansible/$1
# Use secPAge.yml in  resources for example
# ansible-playbook --vault-password-file /home/$USER/vault-pw /home/ansible/secPage.yml
