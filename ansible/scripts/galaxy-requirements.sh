#!/bin/bash
# -s API-Server
#
# --vault-password-file /home/$USER/token  --vault-id
# --check
# --list-hosts --list-tags
# --skip-tags
# --syntax-check
# --tags
# --start-at-task ''
# Gitlab
#  gitlab-user:
#    - tobkern1980
# passphrase
#
ansible-galaxy install -f -vvv --ignore-errors -r ~/.ansible/roles/requirements.yml