#!/bin/bash
# -s API-Server
#
# --vault-password-file /home/$USER/token  --vault-id
# --check
# --list-hosts --list-tags
# --skip-tags
# --syntax-check
# --tags
# --start-at-task 'ansible-role-postfix : Create postfix configuration main.cf via template'
# Gitlab
#  gitlab-user:
#    - tobkern1980
# passphrase
#
# actual error:
# TASK [ansible-role-postfix : Create postfix configuration main.cf via template]
ACTIV_GITBRANCH=$(git branch | grep '*'| cut -d'*' -f 2)

ansible-galaxy install -f -vvv --ignore-errors -r /home/$USER/ansible/roles/test-roles.yml &&\
ansible-playbook ./playbooks/myserver.yml --vault-password-file /home/$USER/googletoken.yml --check
