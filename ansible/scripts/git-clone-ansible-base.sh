#!/bin/bash

PWD_ROLES=$(pwd)
ROLES_DIR=$PWD_ROLES/roles

#read lines 

#< ansible-repos.list | while read line; do
#  echo $line
#done

repos=(
git clone https://github.com/kernt/ansible-vim.git $ROLES_DIR/ansible-vim
git clone https://github.com/geerlingguy/ansible-role-kubernetes.git $ROLES_DIR/ansible-role-kubernetes
git clone https://github.com/kernt/ansible-role-postfix.git $ROLES_DIR/ansible-role-postfix
git clone https://github.com/kernt/ansible-role-fail2ban.git $ROLES_DIR/ansible-role-fail2ban
git clone https://github.com/kernt/ansible-role-users.git $ROLES_DIR/ansible-role-users
git clone https://github.com/geerlingguy/ansible-role-docker.git $ROLES_DIR/ansible-role-docker
git clone https://github.com/geerlingguy/php-apache-container.git $ROLES_DIR/php-apache-container
git clone https://github.com/geerlingguy/ansible-role-docker_arm.git $ROLES_DIR/ansible-role-docker_arm
git clone https://github.com/kernt/ansible-role-zsh.git $ROLES_DIR/ansible-role-zsh
git clone https://github.com/kernt/ansible-role-zerotierone.git $ROLES_DIR/ansible-role-zerotierone
git clone https://github.com/kernt/ansible-docker-compose.git $ROLES_DIR/ansible-docker-compose
git clone https://github.com/geerlingguy/ansible-role-java.git $ROLES_DIR/ansible-role-java
git clone https://github.com/geerlingguy/ansible-role-pip.git $ROLES_DIR/ansible-role-pip
git clone https://github.com/geerlingguy/ansible-role-jenkins.git $ROLES_DIR/ansible-role-jenkins
git clone https://github.com/geerlingguy/ansible-role-filebeat.git $ROLES_DIR/ansible-role-filebeat
git clone https://github.com/geerlingguy/ansible-role-logstash.git $ROLES_DIR/ansible-role-logstash
git clone https://github.com/kernt/ansible-prometheus.git $ROLES_DIR/ansible-prometheus
git clone https://github.com/geerlingguy/ansible-role-elasticsearch.git $ROLES_DIR/ansible-role-elasticsearch
git clone https://github.com/kernt/ansible-role-zsh.git $ROLES_DIR/ansible-role-zsh
git clone https://github.com/kernt/ansible-tmux.git $ROLES_DIR/ansible-tmux
git clone https://github.com/geerlingguy/ansible-role-docker.git $ROLES_DIR/ansible-role-docker
git clone https://github.com/geerlingguy/ansible-role-composer.git $ROLES_DIR/ansible-role-composer
git clone https://github.com/geerlingguy/ansible-role-git.git $ROLES_DIR/ansible-role-git
git clone https://github.com/geerlingguy/ansible-role-gogs.git $ROLES_DIR/ansible-role-gogs
git clone https://github.com/geerlingguy/ansible-role-github-users.git $ROLES_DIR/ansible-role-github-users
git clone https://github.com/geerlingguy/ansible-role-certbot.git $ROLES_DIR/ansible-role-certbot
git clone https://github.com/robertdebock/ansible-role-fail2ban.git $ROLES_DIR/ansible-role-fail2ban
git clone https://github.com/geerlingguy/ansible-role-nginx.git $ROLES_DIR/ansible-role-nginx
git clone https://github.com/geerlingguy/ansible-role-apache.git $ROLES_DIR/ansible-role-apache
git clone https://github.com/geerlingguy/ansible-role-pimpmylog.git $ROLES_DIR/ansible-role-pimpmylog
git clone https://github.com/geerlingguy/ansible-role-htpasswd.git $ROLES_DIR/ansible-role-htpasswd
git clone https://github.com/geerlingguy/ansible-role-mysql.git $ROLES_DIR/ansible-role-mysql
git clone https://github.com/geerlingguy/ansible-role-php-mysql.git $ROLES_DIR/ansible-role-php-mysql
git clone https://github.com/geerlingguy/ansible-role-php-pgsql.git $ROLES_DIR/ansible-role-php-pgsql
git clone https://github.com/geerlingguy/ansible-role-php.git $ROLES_DIR/ansible-role-php
git clone https://github.com/geerlingguy/ansible-role-php-versions.git $ROLES_DIR/ansible-role-php-versions
git clone https://github.com/geerlingguy/ansible-role-apache-php-fpm.git $ROLES_DIR/ansible-role-apache-php-fpm
git clone https://github.com/geerlingguy/ansible-role-phpmyadmin.git $ROLES_DIR/ansible-role-phpmyadmin
git clone https://github.com/robertdebock/ansible-role-users.git $ROLES_DIR/ansible-role-users
git clone https://github.com/geerlingguy/ansible-role-pip.git $ROLES_DIR/ansible-role-pip
git clone https://github.com/bertvv/ansible-role-bind.git $ROLES_DIR/ansible-role-bind
git clone https://github.com/PowerDNS/pdns-ansible.git $ROLES_DIR/pdns-ansible
git clone https://github.com/Oefenweb/ansible-keepalived.git $ROLES_DIR/ansible-keepalived
git clone https://github.com/arillso/ansible.sudoers.git $ROLES_DIR/ansible.sudoers
git clone https://github.com/kernt/ansible-tmux.git $ROLES_DIR/ansible-tmux
git clone https://github.com/galaxyproject/ansible-galaxy.git $ROLES_DIR/ansible-galaxy
git clone https://github.com/gluster/gluster-ansible-infra.git $ROLES_DIR/gluster-ansible-infra
git clone https://github.com/gluster/gluster-ansible-cluster.git $ROLES_DIR/gluster-ansible-cluster
git clone https://github.com/gluster/gluster-ansible-features.git $ROLES_DIR/gluster-ansible-features
git clone https://github.com/gluster/gluster-ansible-repositories.git $ROLES_DIR/gluster-ansible-repositories
git clone https://github.com/gluster/gluster-ansible-maintenance.git $ROLES_DIR/gluster-ansible-maintenance
git clone https://github.com/ceph/ceph-ansible.git $ROLES_DIR/ceph-ansible
git clone https://github.com/emptyDir/ansible-role-k3s.git $ROLES_DIR/ansible-role-k3s
)

for repo in repos
  do 
    STR="https://github.com/emptyDir/ansible-role-k3s.git"
    TAIL=${STR##*/}
    REPONAME=${TAIL%.git}
    echo $REPONAME
    # git clone --progress
done