#!/bin/bash
# 
# 
# for i in pull.git 
#  do
#   pull $i 
#  done
#

PWD_ROLES = $(pwd)
ROLES_DIR = $PWD_ROLES/roles

File_REPOS = "./ansible-repos.list"

GET_CHANGED = $(git pull --dry-run | grep -q -v 'Already up-to-date.' && CHANGED="1" && export $CHANGED)

GET_FILE_REPOS () {
        while IFS= read -r line
            do
            echo "$ROLES_DIR/$line"
        done < "$File_REPOS"
}

DIRS=( "$ROLES_DIR/ansible-vim"
 "$ROLES_DIR/ansible-role-kubernetes"
 "$ROLES_DIR/ansible-role-postfix"
 "$ROLES_DIR/ansible-role-fail2ban"
 "$ROLES_DIR/ansible-role-users"
 "$ROLES_DIR/ansible-role-docker"
 "$ROLES_DIR/php-apache-container"
 "$ROLES_DIR/ansible-role-docker_arm"
 "$ROLES_DIR/ansible-role-zsh"
 "$ROLES_DIR/ansible-role-zerotierone"
 "$ROLES_DIR/ansible-docker-compose"
 "$ROLES_DIR/ansible-role-java"
 "$ROLES_DIR/ansible-role-apache"
 "$ROLES_DIR/ansible-role-pimpmylog"
 "$ROLES_DIR/ansible-role-pip"
 "$ROLES_DIR/ansible-role-jenkins"
 "$ROLES_DIR/ansible-role-filebeat"
 "$ROLES_DIR/ansible-role-logstash"
 "$ROLES_DIR/ansible-prometheus"
 "$ROLES_DIR/ansible-role-zsh"
 "$ROLES_DIR/ansible-tmux"
 "$ROLES_DIR/ansible-role-docker"
 "$ROLES_DIR/ansible-role-composer"
 "$ROLES_DIR/ansible-role-git"
 "$ROLES_DIR/ansible-role-gogs"
 "$ROLES_DIR/ansible-role-github-users"
 "$ROLES_DIR/ansible-role-certbot"
 "$ROLES_DIR/ansible-role-fail2ban"
 "$ROLES_DIR/ansible-role-mysql"
 "$ROLES_DIR/ansible-role-php"
 "$ROLES_DIR/ansible-role-php-versions"
 "$ROLES_DIR/ansible-role-php-mysql"
 "$ROLES_DIR/ansible-role-phpmyadmin"
 "$ROLES_DIR/ansible-role-users"
 "$ROLES_DIR/ansible-role-pip"
 "$ROLES_DIR/ansible-role-nginx"
 "$ROLES_DIR/ansible-role-bind"
 "$ROLES_DIR/pdns-ansible"
 "$ROLES_DIR/ansible-keepalived"
 "$ROLES_DIR/ansible.sudoers"
 "$ROLES_DIR/ansible-tmux"
 "$ROLES_DIR/ansible-galaxy"
 "$ROLES_DIR/gluster-ansible-infra"
 "$ROLES_DIR/gluster-ansible-cluster"
 "$ROLES_DIR/gluster-ansible-features"
 "$ROLES_DIR/gluster-ansible-repositories"
 "$ROLES_DIR/gluster-ansible-maintenance"
 "$ROLES_DIR/ceph-ansible"
 "$ROLES_DIR/ansible-role-k3s"
 "$ROLES_DIR/ansible-role-htpasswd"
 "$ROLES_DIR/ansible-role-mysql")

for dir in "${DIRS[@]}"
    do
        GET_CHANGED=$(git pull --dry-run | grep -q -v 'Already up-to-date.' && CHANGED="1" && export $CHANGED)
        if [ $# -ne 0 ] ; then
            git pull $dir
        else echo Dir $dir: Not CHANGED
        fi
done
