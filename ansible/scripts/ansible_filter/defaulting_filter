# Defaulting Undefined Variables

Jinja2 provides a useful ‘default’ filter that is often a better approach to failing if a variable is not defined:

`{{ some_variable | default(5) }}`

In the above example, if the variable ‘some_variable’ is not defined, the value used will be 5, rather than an error being raised.

If you want to use the default value when variables evaluate to false or an empty string you have to set the second parameter to true:

`{{ lookup('env', 'MY_USER') | default('admin', true) }}`
