#!/usr/bin/env bash
echo "[*] Führe ansible-lint Check durch bei $(pwd)"
$(type -p ansible-lint) myserver.yml
RESULT=$?
[ $RESULT -ne 0 ] && exit 1
exit 0