#!/bin/bash
# -s API-Server
#
# [delete|import|info|init|install|list|login|remove|search|setup]
ansible-playbook $1 --start-at-task "$2"