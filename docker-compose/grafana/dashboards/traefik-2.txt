Simple dashboard for Traefik 2, visualizing every important metric for your Traefik instance(s).

It provides you with statistics to your internals of Traefik, HTTP statics and gives you a brief overview on the health of your traefik instance(s).

Originally this dashobard has been created by ichasco, that can also be found on grafana.com.

Dependencies:

    Grafana 6.4.3
    Graph
    Pie Chart 1.3.9
    Prometheus 1.0.0
    Singlestat 

https://grafana.com/grafana/dashboards/11462
