Requires Proxmox 4.0 or later.

Note: I didn't have any containers running when I made the associated screen shot.

Configure Proxmox to talk to your InfluxDB as per https://pve.proxmox.com/wiki/External_Metric_Server then point this dashboard at the resulting database.

Since Grafana doesn't allow setting limits from a query, this dashboard assumes an 8 core processor and 32GB or ram. If your Proxmox host is different you may have to tune various guages and limits to appropriate values.


Dependencies:

    Grafana 6.1.3
    Clock 1.0.2
    Graph
    InfluxDB 1.0.0
    Singlestat
    Table 

https://grafana.com/grafana/dashboards/10048
