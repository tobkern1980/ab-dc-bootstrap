A simple dashboard to monitor Minio Server.

provide your feedback on: https://github.com/amenezes/grafana-dashboards/issues

prometheus.yml configuration:

  - job_name: minio
    metrics_path: /minio/prometheus/metrics
    scrape_interval: 10s
    static_configs:
    - targets:
      - <your_minio_server>:9000

- require minio server version: RELEASE.2018-05-11T00-29-24Z or later.

Dependencies:

    Grafana 4.4.3
    Graph
    Prometheus 1.0.0
    Singlestat
    Text 

https://grafana.com/grafana/dashboards/6248
